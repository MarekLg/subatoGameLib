package name.panitz.game.example.simple;

import java.util.ArrayList;
import java.util.List;

import name.panitz.game.framework.AbstractGame;
import name.panitz.game.framework.Button;
import name.panitz.game.framework.KeyCode;
import name.panitz.game.framework.SoundObject;
import name.panitz.game.framework.GameObject;
import name.panitz.game.framework.ImageObject;
import name.panitz.game.framework.TextObject;
import name.panitz.game.framework.Vertex;

public class SimpleGame<I,S> extends AbstractGame<I,S>{
    /*+ Zunächst seien die Listen der Spielfiguren definiert. Es gibt einen Hintergrund, eine Liste von Gegnern und als dritte Liste noch ein Paar dekorative Wolken. */
    
  List<GameObject<I>> hintergrund = new ArrayList<>();
  List<GameObject<I>> gegner = new ArrayList<>();
  List<ImageObject<I>> wolken = new ArrayList<>();

    /*+ Der interne Spielzustand sind die bereits erfolgen Stiche durch eine Biene. */
  int stiche = 0;

    /*+ Der Spielzustand wird in einem Textfeld angezeigt. */ 
  TextObject<I> infoText
    = new TextObject<>(new Vertex(30,35)
       ,"Kleines Beispielspiel","Helvetica",28);	

    /*+ Für die erfolgten Stiche erfolgt ein kleines Geräusch. */ 
  SoundObject<S> outch = new SoundObject<>("outch.wav");

    /*+ Es folgt der Konstruktor, in dem alle Spielfiguren initialisiert werden. */  
  public SimpleGame() {
      /*+ Die steuerbare Spielfigur ist ein Bildobjekt und es wird über die Oberklasse initialisiert. */
    super(new ImageObject<>("hexe.png"
        ,new Vertex(200,200),new Vertex(1,1)),800,600);

    /*+ Der Hintergrund ist festes Bild und die Textanzeige des Spielstandes. */
    hintergrund.add(new ImageObject<>("wiese.jpg"));
    hintergrund.add(infoText);

    /*+ Wir erzeugen drei Wolken als Bildobjekte. */
    wolken.add(new ImageObject<>("wolke.png"
        ,new Vertex(800,10),new Vertex(-1,0)));
    wolken.add(new ImageObject<>("wolke.png"
        ,new Vertex(880,90),new Vertex(-1.2,0)));
    wolken.add(new ImageObject<>("wolke.png"
        ,new Vertex(1080,60),new Vertex(-1.1,0)));
    wolken.add(new ImageObject<>("wolke.png"
        ,new Vertex(980,110),new Vertex(-0.9,0)));

    /*+ Und unsere Gegner sind die zwei Bienen. */
    gegner.add(new ImageObject<>("biene.png"
        ,new Vertex(800,100),new Vertex(-1,0)));
    gegner.add(new ImageObject<>("biene.png"
        ,new Vertex(800,300),new Vertex(-1.5,0)));

    /*+ Die drei Listen von Spielfiguren werden dem Spiel zugefügt. */
    getGOss().add(hintergrund);
    getGOss().add(wolken);
    goss.add(gegner);

    /*+ Das Spiel hat drei Knöpfe zur Bedienung. */
    getButtons().add(new Button("Pause", ()-> pause()));
    getButtons().add(new Button("Start", ()-> start()));
    getButtons().add(new Button("Exit", ()-> System.exit(0)));
  }

    /*+ Als nächstes sind die Checks, die nach jedem Tic der Spielschleife durchgeführt werden. */
  @Override
  public void doChecks() {
      /*+ Jede Wolke, die außerhalb des Spielfeldes geflogen ist, wird wieder auf die andere Seite des Spielfeldes gesetzt. */  
    for (GameObject<I> g:wolken){
      if (g.getPos().x+g.getWidth()<0) {
        g.getPos().x = getWidth();
      }
    }

    /*+ Als nächstes weren die Bienen betrachtet.*/
    for (GameObject<I> g:gegner){
	/*+  Zunächst einmal, ob diese auch außerhalb des Spielfeldes geflogen sind. */
      if (g.getPos().x+g.getWidth()<0) {
        g.getPos().x = getWidth();
      }
      /*+ Dann, ob sie die Spielfigur berühren und damit einen Bienenstich auslösen. */
      if (player.touches(g)){
        stiche++;
        infoText.text = "Bienenstiche: "+stiche; 
        g.getPos().moveTo(new Vertex(getWidth()+10,g.getPos().y));
        playSound(outch);
      }
    }
    /*+ Wir beenden das Spiel nach 10 Stichen. (Dann kommt es wohl definitiv zu einer allergischen Reaktion.)*/
    if (stiche>=10){
      gegner.add(new TextObject<>(new Vertex(100,300)
          ,"Du hast verloren","Helvetica",56));
    }
  }

  @Override
  public boolean isStopped() {
    return super.isStopped() || stiche>=10;
  }
    /*+ Es folgen die Reaktionen auf Tastatureingaben. */
  @Override
  public void keyPressedReaction(KeyCode keycode) {
    if (keycode!=null)
     switch (keycode){
      case RIGHT_ARROW:
        getPlayer().getVelocity().move(new Vertex(1,0));
        break;
      case LEFT_ARROW:
        getPlayer().getVelocity().move(new Vertex(-1,0));
        break;
      case DOWN_ARROW:
        getPlayer().getVelocity().move(new Vertex(0,1));
        break;
      case UP_ARROW:
        getPlayer().getVelocity().move(new Vertex(0,-1));
        break;
      default: ;
     }
  }
}

