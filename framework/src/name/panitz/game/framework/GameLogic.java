package name.panitz.game.framework;

import java.util.List;

public interface GameLogic<I,S> extends Movable, Paintable<I> {
  List<List<? extends GameObject<I>>> getGOss();
  GameObject<I> getPlayer();
  List<Button> getButtons();
  
  void playSound(SoundObject<S> so);
  List<SoundObject<S>> getSoundsToPlayOnce();

  double getWidth();
  double getHeight();
	
  void doChecks();
	
  boolean isStopped();
  void keyPressedReaction(KeyCode keycode);
  void keyReleasedReaction(KeyCode keycode);
  
  void start();
  void pause();

    
    /*+ Es folgen einige default-Methoden auf den Spielobjekten. 

Das Zeichnen des gesammten Spiels ist als default-Methode realisiert. Es weren alle Spielobjekte und die Spielfigur gezeichnet. */   
  default public void paintTo(GraphicsTool<I> g){
    for (List<? extends GameObject<I>> gos:getGOss()) 
      for (GameObject<I> go:gos) 
        go.paintTo(g);
    getPlayer().paintTo(g); 
  }
    /*+ Auch die Bewegung wird durch eine default-Methode realisiert und die Eigenbewegung aller Spielfiguren aufgerufen. */
  default void move(){
    if (!isStopped()){
      for (List<? extends GameObject<I>> gos:getGOss())
        for (GameObject<I> go:gos)
          go.move();
      getPlayer().move();
    }
  }    
    /*+ Auch das spielen der noch zu spielenden Geräusche wird hier realsiert. */
  default void playSounds(SoundTool<S> soundTool){
    for (SoundObject<S> so:getSoundsToPlayOnce()){
      so.playSound(soundTool);
    }
    getSoundsToPlayOnce().clear();
  }
}

