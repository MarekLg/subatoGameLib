package name.panitz.game.framework;
/** A simple class for points in a 2-dimsional space. **/ 
public class Vertex {
  public double x;
  public double y;

  public Vertex(double x, double y) {
    this.x = x;
    this.y = y;
  }  
    /*+ Die <tt>toString</tt> Methode, stellt sie als geklammerte Paare dar. */
  @Override
  public String toString() {
    return "("+x+", "+y+")";
  }  

    /*+ Die Methode <tt>move</tt> addiert einen Vektor auf den Punkt auf. Verschiebt ihn also um einen Vektor. */  
  public void move(Vertex that){
    x += that.x;
    y += that.y;
  }
    /*+ Die methode <tt>motoTo</tt> setzt den Punkt komplett auf andere Koordinaten. */
  public void moveTo(Vertex that){
    x = that.x;
    y = that.y;
  }
    /*+ Vom Ursprung lässt sich der Punkt weg skalieren. */
  public Vertex mult(double d) {
    return new Vertex(d*x,d*y);
  }
}

