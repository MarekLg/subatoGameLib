module Main where
import System.Environment
--import Ref
import System.IO

main = do
  args <- getArgs
  sequence_ (map process args)

names fileName
  |null dotExt || not(elem '.' (tail dotExt)) = (name,dotExt)
  |otherwise = ("."++n2,dot2)
  where
    (name,dotExt) = span ('.'/=) fileName
    (n2,dot2) = names (tail dotExt)
    
process fileName = do
  let (name,dotExt) = names fileName
  inputHandle <- openFile fileName ReadMode 
  hSetEncoding inputHandle utf8
  cs <- hGetContents inputHandle
--  cs <- readFile fileName
  print name
  let outF = name++".xml"
  let extension = tail dotExt
  outputHandle <- openFile outF WriteMode
  hSetEncoding outputHandle utf8
  hPutStr outputHandle  "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
  hPutStr outputHandle  "<sourceCode>\n"
  let progName = reverse$takeWhile ('/' /=) $reverse name
  hPutStr outputHandle  ("<code class=\""++progName++"\" lang=\""++extension++"\"><![CDATA[\n")
  transform outputHandle progName extension cs
  hPutStr outputHandle  "\n]]></code>"
  hPutStr outputHandle  "\n</sourceCode>"
  print ("ready:" ++ fileName)
  hClose outputHandle

--xml Documentation starts with /*+
transform outF progName extension ('/':'*':'+':cs) = do
  hPutStr outF "\n]]></code>"
  hPutStr outF "\n"
  let (latex,rest) = takeToCommentEnd cs
  hPutStr outF latex
  hPutStr outF "\n"
  hPutStr outF ("<code class=\""++progName++"\" lang=\""++extension++"\"><![CDATA[\n")
  transform outF progName extension rest
--Javadoc starts with /** and is skipped
transform outF progName extension ('/':'*':'*':cs) = do
  let (_,rest) = takeToCommentEnd cs
  transform outF progName extension rest
transform outF progName extension (c:cs) = do
  hPutStr outF (if c=='\t' then "  " else [c])
  transform outF progName extension cs 
transform outF progName extension "" = return ()

takeToCommentEnd ('*':'/':xs) = ("",xs)
takeToCommentEnd ('\t':cs) = (' ':' ':ys,xs)
  where
    (ys,xs)  = takeToCommentEnd cs
takeToCommentEnd (c:cs) = (c:ys,xs)
  where
    (ys,xs)  = takeToCommentEnd cs
takeToCommentEnd"" = ("","")
